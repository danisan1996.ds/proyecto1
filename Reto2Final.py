
datos_persona1: dict = {
    "nombre_cliente":"A",
    "apellido" :"B",
    "tarjeta_virutal":1,
    "hiphop":1,
    "underground":1,
    "rap":1,
    "gangsta_rap":1,
    "edad":24
}

def recomendaciones_musicales(datos_persona1:dict)->dict:
    Nombre = datos_persona1["nombre"]
    Apellido = datos_persona1["apellido"]
    
    diccionario_respuesta = {
        "usuario" : Nombre + " " + Apellido
    }
    valorpagar : int = 2000
    
    if datos_persona1["edad"] >= 30:
        if datos_persona1["hiphop"] == 1:
            if datos_persona1["rap"] == 1:
                if datos_persona1["underground"] == 1:
                    if datos_persona1["gangsta_rap"] == 1:
                        diccionario_respuesta["recomendación"] = "A tribe called quest"
                        diccionario_respuesta["valor_pagar"] = valorpagar
                    else:
                        diccionario_respuesta["recomendación"] = "Inspiración divina"
                        diccionario_respuesta["valor_pagar"] = valorpagar
                else:
                    diccionario_respuesta["recomendación"] = "Lástima"
                    diccionario_respuesta["valor_pagar"] = valorpagar
            else:
                diccionario_respuesta["recomendación"] = "Pecador"
                diccionario_respuesta["valor_pagar"] = valorpagar*0.5
        else:
            if datos_persona1["gangsta_rap"] == 1:
                diccionario_respuesta["recomendación"] = "The message"
                diccionario_respuesta["valor_pagar"] = valorpagar
            else:
                diccionario_respuesta["recomendación"] = "Latinoamérica"
                diccionario_respuesta["valor_pagar"] = valorpagar
    else:
        if datos_persona1["hiphop"] == 1:
            if datos_persona1["rap"] == 1:
                if datos_persona1["underground"] == 1:
                    if datos_persona1["gangsta_rap"] == 1:
                        diccionario_respuesta["recomendación"] = "Made me do it"
                        diccionario_respuesta["valor_pagar"] = valorpagar*0.6
                    else:
                        diccionario_respuesta["recomendación"] = "Eternamente"
                        diccionario_respuesta["valor_pagar"] = valorpagar
                else:
                    diccionario_respuesta["recomendación"] = "Es épico"
                    diccionario_respuesta["valor_pagar"] = valorpagar
            else:
                diccionario_respuesta["recomendación"] = "Las esenciales"
                diccionario_respuesta["valor_pagar"] = valorpagar
        else:
            diccionario_respuesta["recomendación"] = "Ojos color de sol"
            diccionario_respuesta["valor_pagar"] = valorpagar
    
    return diccionario_respuesta

print(recomendaciones_musicales(datos_persona1))