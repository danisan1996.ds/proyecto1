
datos : list = [
    {
        "tipo_plan" : "fijo",
        "valor_pagar" : 3
    },
    {
        "tipo_plan" : "fijo",
        "valor_pagar" : 7
    },
    {
        "tipo_plan" : "fijo",
        "valor_pagar" : 4
    },
    {
        "tipo_plan" : "fijo",
        "valor_pagar" : 0
    },
    {
        "tipo_plan" : "fijo",
        "valor_pagar" : 7
    },
    {
        "tipo_plan" : "fijo",
        "valor_pagar" : 4
    }
]

def calcular_ingresos(datos:list)->dict:

    suma_fijo : int = 0
    suma_celular : int = 0
    total : int = 0
    contador_fijo = 0
    contador_celular = 0

    for x in range(0,len(datos)):
        if (datos[x]["tipo_plan"]) == "fijo":
            suma_fijo += datos[x]["valor_pagar"]
            total += datos[x]["valor_pagar"]
            contador_fijo += 1
            
        elif (datos[x]["tipo_plan"]) == "celular":
            suma_celular += datos[x]["valor_pagar"]
            total += datos[x]["valor_pagar"]
            contador_celular += 1
    
    if contador_fijo != 0:
        promedioFijo : int = suma_fijo / contador_fijo
        promedio_fijo = round(promedioFijo,1)
    else:
        promedio_fijo = 0
    
    if contador_celular != 0:
        promedioCelular : int = suma_celular / contador_celular
        promedio_celular = round(promedioCelular,1)
    else:
        promedio_celular = 0

    resultado = {
        "total" : total,
        "promedio_fijo" : promedio_fijo,
        "promedio_celular" : promedio_celular
    }
    return (resultado)

print(calcular_ingresos(datos))
