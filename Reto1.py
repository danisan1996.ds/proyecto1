
def promedio_contagios(region:str,contagios_lunes:int,contagios_martes:int,contagios_miercoles:int,contagios_jueves:int,contagios_viernes:int, contagios_sabado:int, contagios_domingo:int)->str:
    nombre_region : str = str(region)
    promedio : int = int(contagios_lunes+contagios_martes+contagios_miercoles+contagios_jueves+contagios_viernes+contagios_sabado+contagios_domingo)/7
    return "El promedio de contagios semanal por covid-19 en {} es de {:.1f} personas.".format(nombre_region,promedio)

print(promedio_contagios("Antioquia",300,240,100,210,450,530,220))
