
def estudio_covid(ruta_datos: str)->dict:

    import pandas

    covid = pandas.read_csv(ruta_datos)

    contadorF = 0
    contadorM = 0
    muertosF = 0
    muertosM =0
    sexos = ['F','M']


    for x in range(len(covid["Sexo"])):
        if covid["Sexo"][x] == 'F':
            contadorF += 1
            if covid["Estado"][x] == 'Fallecido':
                muertosF += 1
        else:
            contadorM += 1
            if covid["Estado"][x] == 'Fallecido':
                muertosM += 1
    contagios = [contadorF,contadorM]
    fallecidos = [muertosF,muertosM]

    if contadorF > contadorM:
        diccionario = {
            'sexo_mayor_contagio' : sexos[0],
            'cantidad_contagios' : contagios[0],
            'fallecidos' : fallecidos[0]
        }
    else:
        diccionario = {
            'sexo_mayor_contagio' : sexos[1],
            'cantidad_contagios' : contagios[1],
            'fallecidos' : fallecidos[1]
        }

    return diccionario

"Retos/Esto/Debería/Funcionar/casos_covid.csv"

print(estudio_covid("Retos/Esto/Debería/Funcionar/casos_covid.csv"))